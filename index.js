console.log("Hello World");

function displaySum(num1,num2){
	let sum = num1 + num2;
	console.log("Displayed sum of: " + num1 + " and " + num2);
	console.log(sum);
}
displaySum(99,1);



function displayDifference(num1,num2){
	let difference = num1 - num2;
	console.log("Displayed difference of: " + num1 + " and " + num2);
	console.log(difference);
}
displayDifference(100,1);


function multiplyNumbers(num1,num2){
	let multiply = num1 * num2;
	console.log("The product of " + num1 + " and " + num2 + ":");
	return multiply;
}
let product = multiplyNumbers(2,11);
console.log(product);


function divideNumbers(num1,num2){
	let divide = num1 / num2;
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return divide;
}
let quotient = divideNumbers(90,5);
console.log(quotient);


function areaOfCircle(radius){
	const pi = 3.14;
	let area = pi * radius * radius;
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return area;
}
let circleArea = areaOfCircle(15);
console.log(circleArea);


function averageOfFourNos(n1, n2, n3, n4){
	let average = (n1 + n2 + n3 + n4) / 4;
	console.log("The average of " + n1 + ", " + n2 + ", " + n3 + ", " + n4 + ":");
	return average;
}
let averageVar= averageOfFourNos(11,22,33,44);
console.log(averageVar);


function scoreIfPassOrFail(n1, n2){
	let percentage = n1 / n2;
	let isPassed = percentage >= 0.5;
	console.log("Is " + n1 + "/" + n2 + " a passing score?");
	return isPassed;
}
let isPassingScore = scoreIfPassOrFail(60,100);
console.log(isPassingScore);